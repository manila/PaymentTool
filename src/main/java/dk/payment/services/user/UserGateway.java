package dk.payment.services.user;

import dk.payment.services.entities.User;

public interface UserGateway {
	User addUser(User user);
	User getUser(String email);
	User getUserByToken(String token);
}
