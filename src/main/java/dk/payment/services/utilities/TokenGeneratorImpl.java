package dk.payment.services.utilities;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import dk.payment.services.user.TokenGenerator;
import dk.payment.services.user.TokenGenerator.InvalidNumberOfTokensException;

public class TokenGeneratorImpl implements TokenGenerator {

	@Override
	public List<String> generate(String tokenType, int numberOfTokens) {
		if(numberOfTokens <= 0 || numberOfTokens > 5)
			throw new InvalidNumberOfTokensException();
		if (tokenType.equals("basic")) {
			List<String> tokens = new ArrayList<>();
			for(int i=0; i < numberOfTokens; i++)
				tokens.add(UUID.randomUUID().toString());
			return tokens;
		} else
		return null;
	}

}
