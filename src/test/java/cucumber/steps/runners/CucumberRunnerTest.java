package cucumber.steps.runners;
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/resourses/", glue="cucumber/steps/" )
public class CucumberRunnerTest {

}
